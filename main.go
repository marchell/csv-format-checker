package main

import (
	"encoding/csv"
	"fmt"
	"github.com/jszwec/csvutil"
	"io"
	"log"
	"os"
)

type Row struct {
	VoucherCode        string            `csv:"voucher_code"`
	CompletionDatetime string            `csv:"completion_datetime"`
	Nilai              string            `csv:"nilai"`
	RatingScore        string            `csv:"rating_score"`
	UlasanText         string            `csv:"ulasan_text"`
	OtherData          map[string]string `csv:"-"`
}

func main() {
	// 1. Open the file
	filename := ""
	if len(os.Args) > 1 {
		filename = os.Args[1]
	} else {
		filename = "check.csv"
	}
	recordFile, err := os.Open(filename)
	if err != nil {
		fmt.Println("silahkan coba lagi dengan mengetik di terminal: './csv-format-checker-windows-386.exe C:/Downloads/csv-format-checker/check.csv' : ",err)
		fmt.Scanln() // wait for Enter Key

		os.Exit(1)

	}
	// 2. Initialize the reader
	reader := csv.NewReader(recordFile)

	decoder, err := csvutil.NewDecoder(reader)
	if err != nil {
		log.Fatal(err)
	}

	headers := decoder.Header()
	if !contains(headers, "voucher_code") {
		fmt.Printf("header voucher_code ga ada!")
		fmt.Scanln() // wait for Enter Key

		os.Exit(1)
	}
	if !contains(headers, "completion_datetime") {
		fmt.Printf("header completion_datetime ga ada!")
		fmt.Scanln() // wait for Enter Key

		os.Exit(1)
	}
	if !contains(headers, "nilai") {
		fmt.Printf("header nilai ga ada!")
		fmt.Scanln() // wait for Enter Key

		os.Exit(1)
	}
	if !contains(headers, "rating_score") {
		fmt.Printf("header rating_score ga ada!")
		fmt.Scanln() // wait for Enter Key

		os.Exit(1)
	}
	if !contains(headers, "ulasan_text") {
		fmt.Printf("header ulasan_text ga ada!")
		fmt.Scanln() // wait for Enter Key

		os.Exit(1)
	}

	for {
		row := &Row{OtherData: make(map[string]string)}

		if err := decoder.Decode(row); err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		if row.Nilai == "" {
			row.printAndExit("nilai")
		}

		if row.VoucherCode == "" {
			row.printAndExit("voucher_code")
		}

		if row.CompletionDatetime == "" {
			row.printAndExit("completion_datetime")
		}

		if row.RatingScore == "" {
			row.printAndExit("rating_score")
		}

		if row.UlasanText == "" {
			row.printAndExit("ulasan_text")
		}
	}

	fmt.Printf("GOOD LUCK! harusnya udah aman")
	fmt.Scanln() // wait for Enter Key
}

func (row Row) printAndExit(fieldName string) {
	fmt.Printf("%s :: kolom %s kosong", row.VoucherCode, fieldName)
	fmt.Scanln() // wait for Enter Key
	os.Exit(1)
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
